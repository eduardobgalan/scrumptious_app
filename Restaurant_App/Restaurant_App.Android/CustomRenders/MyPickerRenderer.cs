﻿using Android.Graphics.Drawables;
using Restaurant_App.Droid.CustomRenders;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Picker), typeof(MyPickerRenderer))]
namespace Restaurant_App.Droid.CustomRenders
{
    public class MyPickerRenderer : PickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                // padding
                this.Control.SetPadding(15, 5, 5, 10);
                // border and bg
                GradientDrawable customBG = new GradientDrawable();
                customBG.SetColor(Android.Graphics.Color.Transparent);
                customBG.SetCornerRadius(3);
                int borderWidth = 2;
                customBG.SetStroke(borderWidth, Android.Graphics.Color.LightGray);
                this.Control.SetBackground(customBG);
            }
        }
    }
}