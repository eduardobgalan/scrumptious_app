﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;

namespace Restaurant_App.Droid
{
    [Activity(Label = "Restaurant_App", Icon = "@drawable/icon", Theme = "@style/splashscreen", MainLauncher = false, NoHistory = true)]
    public class SplashActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            StartActivity(typeof(MainActivity));
        }
    }
}