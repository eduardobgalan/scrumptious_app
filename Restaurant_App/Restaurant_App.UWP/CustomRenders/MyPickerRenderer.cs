﻿using Restaurant_App.UWP.CustomRenders;
using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;

[assembly: ExportRenderer(typeof(Picker), typeof(MyPickerRenderer))]
namespace Restaurant_App.UWP.CustomRenders
{
    public class MyPickerRenderer : PickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.BorderThickness = new Windows.UI.Xaml.Thickness(0.5, 0.5, 0.5, 0.5);
                Control.Padding = new Windows.UI.Xaml.Thickness(10, 5, 5, 10);
                Control.VerticalContentAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
            }
        }
    }
}