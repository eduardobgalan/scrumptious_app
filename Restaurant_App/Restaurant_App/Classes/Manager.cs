﻿using Restaurant_App.Enums;
using Restaurant_App.Interfaces;
using Restaurant_App.Models;
using Restaurant_App.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Restaurant_App.Classes
{
    public class Manager
    {
        public IRestService RestService;

        public Manager(IRestService restService)
        {
            RestService = restService;
        }

        #region Login module
        public Task<User> Login(LoginViewModel model)
        {
            return RestService.Login(model);
        }

        public Task<User> CreateUser(User model)
        {
            return RestService.CreateUser(model);
        }

        public Task<bool> RecoverPassword(LoginViewModel model)
        {
            return RestService.RecoverPassword(model);
        }
        #endregion Login module

        #region Places module
        public Task<Place> ManagePlace(Place model)
        {
            return RestService.ManagePlace(model);
        }

        public Task<List<Api_PlaceViewModel>> GetPlaces(int usrId)
        {
            return RestService.GetPlaces(usrId);
        }

        public Task<List<Category>> GetCategories()
        {
            return RestService.GetCategories();
        }
        #endregion Places module

        #region Orders module
        public Task<Order> CreateOrder(Order model)
        {
            return RestService.CreateOrder(model);
        }

        public Task<List<Product>> GetProducts()
        {
            return RestService.GetProducts();
        }

        public Task<Order> GetOrderbyId(int orderId)
        {
            return RestService.GetOrderById(orderId);
        }

        public Task<List<Order>> GetOrdersByPlaceId(int placeId)
        {
            return RestService.GetOrdersByPlaceId(placeId);
        }

        public Task<bool> ChangeOrderStatus(Order order)
        {
            return RestService.ChangeOrderStatus(order);
        }

        public Task<Message> SetOrderMessage(Message model)
        {
            return RestService.SetOrderMessage(model);
        }

        public Task<bool> SetOrderReport(Report model)
        {
            return RestService.SetOrderReport(model);
        }

        public Task<List<Message>> GetOrderMessages(int orderId)
        {
            return RestService.GetOrderMessages(orderId);
        }
        #endregion Orders module
    }
}
