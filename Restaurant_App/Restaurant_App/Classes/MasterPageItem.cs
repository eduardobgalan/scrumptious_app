﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Restaurant_App.Classes
{
    public class MasterPageItem
    {
        public string Title { get; set; }
        public string Icon { get; set; }
        public Type TargetType { get; set; }
    }
}
