﻿using Xamarin.Forms;

namespace Restaurant_App.Effects
{
    public class ClearEntryEffect : RoutingEffect
    {
        public ClearEntryEffect() : base("Effects.ClearEntryEffect")
        {
        }
    }
}
