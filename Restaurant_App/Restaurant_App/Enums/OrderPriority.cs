﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Restaurant_App.Enums
{
    public enum OrderPriority
    {
        [Description("Low")]
        Low = 1,
        [Description("Medium")]
        Medium = 2,
        [Description("High")]
        High = 3
    }
}
