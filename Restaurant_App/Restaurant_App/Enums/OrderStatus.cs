﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Restaurant_App.Enums
{
    public enum OrderStatus
    {
        [Description("New")]
        New = 1,
        [Description("Pending")]
        Pending = 2,
        [Description("On the go")]
        InTransit = 3,
        [Description("Completed")]
        Completed = 4,
        [Description("Error")]
        Error = 10
    }
}
