﻿namespace Restaurant_App.Enums
{
    public enum ProductStatus
    {
        Active = 1,
        Inactive = 2,
        SoldOut = 3
    }
}
