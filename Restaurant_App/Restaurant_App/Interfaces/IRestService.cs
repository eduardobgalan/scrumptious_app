﻿using Restaurant_App.Enums;
using Restaurant_App.Models;
using Restaurant_App.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Restaurant_App.Interfaces
{
    public interface IRestService
    {
        #region Login
        Task<User> Login(LoginViewModel model);
        Task<User> CreateUser(User model);
        Task<bool> RecoverPassword(LoginViewModel model);
        #endregion Login

        #region Places module
        Task<Place> ManagePlace(Place model);
        Task<List<Api_PlaceViewModel>> GetPlaces(int usrId);
        Task<List<Category>> GetCategories();
        #endregion Places module

        #region Orders module
        Task<Order> CreateOrder(Order model);
        Task<List<Product>> GetProducts();
        Task<Order> GetOrderById(int orderId);
        Task<List<Order>> GetOrdersByPlaceId(int placeId);
        Task<bool> ChangeOrderStatus(Order order);
        Task<Message> SetOrderMessage(Message model);
        Task<bool> SetOrderReport(Report model);
        Task<List<Message>> GetOrderMessages(int orderId);
        #endregion Orders module
    }
}
