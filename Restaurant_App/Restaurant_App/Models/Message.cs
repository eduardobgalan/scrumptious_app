﻿using System;

namespace Restaurant_App.Models
{
    public class Message
    {
        public Int32 MessageId { get; set; }

        public String Content { get; set; }
        public String Image { get; set; }
        public String Date { get; set; }

        public DateTime CreationDate { get; set; }

        public bool IsReport { get; set; }
        public bool Img_IsVisible { get; set; }
        public bool Txt_IsVisible { get; set; }

        public byte[] ImageBtm { get; set; }

        public Int32 UserId { get; set; }
        public virtual User User { get; set; }
        public Int32 OrderId { get; set; }
        public virtual Order Order { get; set; }

        // Para uso en pantalla.
        public Int32 ImgWidth { get; set; }
        public Xamarin.Forms.LayoutOptions Position { get; set; }
    }
}
