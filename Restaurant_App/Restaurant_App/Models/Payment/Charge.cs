﻿using Newtonsoft.Json;
using System;

namespace Restaurant_App.Models.Payment
{
    public class Charge
    {
        public string ExpirationDate { get; set; }
        public string Number { get; set; }
        public string Currency { get; set; }
        public string Cvc { get; set; }
        public decimal Amount { get; set; }
    }

    public class PaymentModel
    {
        public string Token { get; set; }
        public decimal Amount { get; set; }
    }
}
