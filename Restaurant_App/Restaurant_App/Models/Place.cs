﻿using System;
using System.Collections.Generic;
using System.Net.Http;

namespace Restaurant_App.Models
{
    public class Place
    {
        public Int32 PlaceId { get; set; }
        public Int32 UserId { get; set; }
        public virtual User User { get; set; }
        public String Address { get; set; }
        public String Note { get; set; }
        public String Name { get; set; }
        public String Image { get; set; }
        public String ZipCode { get; set; }
        public Decimal Latitude { get; set; }
        public Decimal Longitude { get; set; }
        public Int32 SelectedCounter { get; set; }
        public byte[] ImageBtm { get; set; }
        public List<Order> Orders { get; set; }
        public List<Int32> Categories { get; set; }
        public List<Category> ParentCategories { get; set; }
    }
}
