﻿using Restaurant_App.Classes;
using Restaurant_App.Enums;
using System;

namespace Restaurant_App.Models
{
    public class User
    {
        public Int32 UserId { get; set; }
        public String Name { get; set; }
        public String LastName { get; set; }
        public String Email { get; set; }
        public String Password { get; set; }
        public DateTime CreationDate { get; set; }
        public UserStatus Status { get; set; }
        public String Token { get; set; }
        
        // Para uso en el registro
        public bool _succ { get; set; }
        public JsonResultModel _err { get; set; }
    }
}
