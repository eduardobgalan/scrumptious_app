﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Restaurant_App.ViewModels
{
    public class LoginViewModel
    {
        public String Email { get; set; }
        public String Password { get; set; }
    }
}
