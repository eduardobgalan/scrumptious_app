﻿namespace Restaurant_App.ViewModels
{
    public class MessagesRequest
    {
        /// <summary>
        /// Orden actual.
        /// </summary>
        public int OrderId { get; set; }
        /// <summary>
        /// Filtro para mensajes o reportes.
        /// </summary>
        public bool IsReport { get; set; }
    }
}
