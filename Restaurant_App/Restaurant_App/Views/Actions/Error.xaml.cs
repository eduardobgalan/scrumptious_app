﻿using Xamarin.Forms;

namespace Restaurant_App.Views.Actions
{
    public partial class Error : ContentPage
    {
        public Error()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            this.Title = "OOPS!";
        }
    }
}