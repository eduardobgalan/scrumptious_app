﻿using System;
using Xamarin.Forms;

namespace Restaurant_App.Views.Actions
{
    public partial class View_Image : ContentPage
    {
        public View_Image()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            Image.Source = (string)this.BindingContext;
            Image.HeightRequest = this.Height * 0.50;
            Image.GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(() => { Navigation.PopModalAsync(true); }) });

            this.BackgroundColor = Color.Black;
        }

        async void On_Back(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync(true);
        }
    }
}