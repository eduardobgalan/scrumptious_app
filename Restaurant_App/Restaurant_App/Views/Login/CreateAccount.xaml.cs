﻿using Restaurant_App.Models;
using System;
using System.Linq;
using Xamarin.Forms;

namespace Restaurant_App.Views
{
    public partial class CreateAccount : ContentPage
    {
        public CreateAccount()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            this.BindingContext = new User();
            SignInLabel.GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(() => { Sing_In(); }) });
        }

        async void Sing_In()
        {
            while (Navigation.ModalStack.Any())
            {
                await Navigation.PopModalAsync();
            }

            if (Navigation.NavigationStack.Any())
                await Navigation.PopToRootAsync();
        }

        async void On_Create(object sender, EventArgs e)
        {
            var btn = (Button)sender;

            var model = (User)BindingContext;

            var user_created = await App.Manager.CreateUser(model);

            if (user_created._succ)
            {
                while (Navigation.ModalStack.Any())
                {
                    await Navigation.PopModalAsync();
                }

                if (Navigation.NavigationStack.Any())
                    await Navigation.PopToRootAsync();
            }
            else
                await DisplayAlert("Oops", user_created._err.Model, "Close");
        }
    }
}
