﻿using Restaurant_App.ViewModels;
using System;
using System.Linq;
using Xamarin.Forms;

namespace Restaurant_App.Views
{
    public partial class RecoverPassword : ContentPage
    {
        public RecoverPassword()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            this.BindingContext = new LoginViewModel();
        }

        async void Get_Back(object sender, EventArgs e)
        {
            var btn = (Button)sender;

            Device.BeginInvokeOnMainThread(() => { btn.Text = "Redirecting..."; });

            while (Navigation.ModalStack.Any())
            {
                await Navigation.PopModalAsync();
            }

            if (Navigation.NavigationStack.Any())
                await Navigation.PopToRootAsync();
        }

        async void On_Recover(object sender, EventArgs e)
        {
            var model = (LoginViewModel)BindingContext;

            var usr_recover = await App.Manager.RecoverPassword(model);

            if (usr_recover)
            {
                while (Navigation.ModalStack.Any())
                {
                    await Navigation.PopModalAsync();
                }

                if (Navigation.NavigationStack.Any())
                    await Navigation.PopToRootAsync();

                await DisplayAlert("Done", "Check your email inbox", "Close");
            }
            else
                await DisplayAlert("Oops", "something went wrong. Please try again", "Close");
        }
    }
}
