﻿using Restaurant_App.Models;
using Restaurant_App.ViewModels;
using Restaurant_App.Views.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Restaurant_App.Views.Order
{
    public partial class AddProducts : ContentPage
    {
        List<ProductViewModel> productsVM = new List<ProductViewModel>();
        //List<Category> categories = new List<Category>();
        //Dictionary<int, string> getCategories = new Dictionary<int, string>();
        OrderViewModel order = new OrderViewModel();

        public AddProducts()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            this.Title = "CATALOG";
            this.BackgroundColor = Color.White;

            productsVM = ((OrderViewModel)BindingContext).Products;

            searchBtn.GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(() => { On_Filter(); }) });
            clearBtn.GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(() => { On_Clear(); }) });

            var products = productsVM.Where(x => x.IsVisible).OrderByDescending(x => x.Qty).ToList();
            if (products.Any())
                ProductsList.ItemsSource = products;
            else
            {
                await Task.Run(async () =>
                {
                    await Task.Delay(300);
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        var destination = new Error();

                        var backFunction = new ToolbarItem()
                        {
                            Text = "Back",
                            Icon = "Content/Icono_Back.png",
                        };

                        backFunction.Command = new Command(() => { On_Back(); });

                        destination.ToolbarItems.Add(backFunction);
                        destination.Title = "OOPS";
                        destination.BindingContext = BindingContext;
                        Application.Current.MainPage = new MainPage(destination);
                    });
                });
            }
        }

        async void On_Place(object sender, EventArgs e)
        {
            var model = (OrderViewModel)this.BindingContext;
            var products = model.Products.Where(x => x.Qty > 0).ToList();
            List<OrderProduct> OP = new List<OrderProduct>();

            if (model.Order != null)
            {
                model.Order.CreationDate = DateTime.Now;
                model.Order.DesiredDate = DateTime.Now;
            }
            else
            {
                model.Order = new Models.Order()
                {
                    CreationDate = DateTime.Now,
                    DesiredDate = DateTime.Now,
                };
            }

            if (products.Count > 0)
            {
                foreach (var item in products)
                {
                    item.Total = item.Qty * item.Product.Price;
                    var unitId = item.Product.Units;

                    var content = new OrderProduct
                    {
                        ProductId = item.Product.ProductId,
                        Product = item.Product,
                        Quantity = item.Qty,
                        Price = item.Product.Price,
                        Total = item.Qty * item.Product.Price,
                        UnitId = item.UnitId
                    };
                    OP.Add(content);
                }

                model.Order.OrderProducts = OP;

                var destination = new CreateOrder();
                destination.BindingContext = model;
                destination.Title = "NEW ORDER";
                Application.Current.MainPage = new MainPage(destination);
            }
            else
            {
                await DisplayAlert("Ops", "You must select at least one product.", "Accept");
            }
        }

        private void On_Back(object sender, EventArgs e)
        {
            On_Back();
        }

        void On_Unit_Selected(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            var unit = (Unit)picker.SelectedItem;

            var product = (ProductViewModel)picker.BindingContext;
            product.UnitId = unit.UnitId;
            product.Unit = unit;
        }

        void On_Add(object sender, EventArgs e)
        {
            var btn = (Button)sender;

            var model = (ProductViewModel)btn.BindingContext;

            var product = (ProductViewModel)productsVM.SingleOrDefault(x => x.Product.ProductId == model.Product.ProductId);

            if (product.Qty < product.Quantity)
            {
                product.Qty += 1;
            }
        }

        void On_Remove(object sender, EventArgs e)
        {
            var btn = (Button)sender;

            var model = (ProductViewModel)btn.BindingContext;

            var product = (ProductViewModel)productsVM.SingleOrDefault(x => x.Product.ProductId == model.Product.ProductId);

            if (product.Qty > 0)
            {
                product.Qty -= 1;
            }
        }

        //void On_Picker_Selected(object sender, EventArgs e)
        //{
        //    Filter.Text = "";
        //    var cat = categories[Category.SelectedIndex];
        //    ProductsList.ItemsSource = (List<ProductViewModel>)productsVM.Where(x => x.Product.CategoryId == cat.CategoryId).ToList();
        //}

        /// <summary>
        /// Funcion para filtrar.
        /// </summary>
        /// 
        void On_Filter()
        {
            //Category.SelectedIndex = 0;
            var str = Filter.Text;
            ProductsList.ItemsSource = null;
            ProductsList.ItemsSource = (List<ProductViewModel>)productsVM.Where(x => x.Product.Name.Contains(str)).ToList();
        }

        /// <summary>
        /// Funcion para limpiar el filtro.
        /// </summary>
        void On_Clear()
        {
            Filter.Text = "";
            //Category.SelectedIndex = 0;
            ProductsList.ItemsSource = null;
            ProductsList.ItemsSource = (List<ProductViewModel>)productsVM.ToList();
        }

        /// <summary>
        /// Comando que se llama al mostrar la pantalla de Error o el listado de Products.
        /// </summary>
        void On_Back()
        {
            var destination = new SelectCategory();
            destination.BindingContext = BindingContext;
            destination.Title = "CATEGORIES";
            Application.Current.MainPage = new MainPage(destination);
        }
    }
}

// Esto estaba dentro de OnAppearing()
//products = await App.Manager.GetProducts();
//categories = await App.Manager.GetCategories();

//foreach (var item in products)
//{
//    item.Name = item.Name.ToUpper();
//    item.Image = imageHost + item.Image;
//    var model = new ProductViewModel
//    {
//        Product = item,
//        Quantity = 10
//    };

//    productsVM.Add(model);
//}
//order.Products = productsVM;

//getCategories = categories.ToDictionary(i => i.CategoryId, i => i.Name);

//Category.Items.Add("Category");  // Creamos la opcion 'Category'
//foreach (var item in getCategories)
//{
//    Category.Items.Add(item.Value);
//}
//Category.SelectedIndex = 0; // Seleccionamos el primer elemento

//var currBinding = (OrderViewModel)this.BindingContext;
//if (currBinding != null)
//{
//    var orderToClone = (OrderViewModel)this.BindingContext;

//    foreach (var _prod in orderToClone.Order.OrderProducts) // Recorremos los detalles actuales
//    {
//        var prod = order.Products.SingleOrDefault(x => x.Product.ProductId == _prod.ProductId); // Buscamos el producto en la lista
//        prod.Qty = _prod.Quantity; // Cambiamos la cantidad
//    }

//    orderToClone.Order.CreationDate = DateTime.Now;
//    orderToClone.Order.DesiredDate = DateTime.Now;

//    this.BindingContext = new OrderViewModel()
//    {
//        Order = orderToClone.Order, // Orden a clonar
//        Products = order.Products, // Productos ya actualizados
//    };
//}
//else
//{
//    this.BindingContext = new OrderViewModel()
//    {
//        Order = new Models.Order()
//        {
//            CreationDate = DateTime.Now,
//            DesiredDate = DateTime.Now,
//        },
//        Products = order.Products
//    };
//}
