﻿using System;
using Xamarin.Forms;

namespace Restaurant_App.Views.Order
{
    public partial class NoOrder : ContentPage
    {
        public NoOrder()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            this.Title = "MY PLACE ORDERS";
            this.BackgroundColor = Color.White;
        }

        void New_Order(object sender, EventArgs e)
        {
            var btn = (Button)sender;

            Device.BeginInvokeOnMainThread(() => { btn.Text = "Redirecting..."; });

            var destination = new SelectCategory();
            Application.Current.MainPage = new MainPage(destination);
        }
    }
}
