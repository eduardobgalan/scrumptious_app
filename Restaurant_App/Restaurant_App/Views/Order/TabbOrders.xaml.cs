﻿using Restaurant_App.ViewModels;
using Restaurant_App.Views.Actions;
using Restaurant_App.Views.Place;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Restaurant_App.Views.Order
{
    public partial class TabbOrders : TabbedPage
    {
        public TabbOrders()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            var model = (Api_PlaceViewModel)this.BindingContext;

            if (model.Place.Orders != null)
            {
                BuildTab("Pending", "Content/Tab_Pending.png", Enums.OrderStatus.Pending, model);
                BuildTab("In Transit", "Content/Tab_In_Transit.png", Enums.OrderStatus.InTransit, model);
                BuildTab("Completed", "Content/Tab_Completed.png", Enums.OrderStatus.Completed, model);

                BarTextColor = Color.White;
                BarBackgroundColor = Color.FromHex("EB7273");
            }
            else
            {
                await Task.Run(async () =>
                {
                    await Task.Delay(300);
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        var destination = new Error();
                        var backFunction = new ToolbarItem()
                        {
                            Text = "Back",
                            Icon = "Content/Icono_Back.png",
                        };

                        backFunction.Command = new Command(() => { On_Back(); });

                        destination.ToolbarItems.Add(backFunction);
                        Application.Current.MainPage = new MainPage(destination);
                    });
                });
            }
            this.BarTextColor = Color.White;
        }

        /// <summary>
        /// Función que se asigna al menú navegar al principio de la app.
        /// </summary>
        private void On_Back()
        {
            var destination = new MyPlaces(App.CurrentUser().u_id);
            destination.Title = "MY PLACES";
            Application.Current.MainPage = new MainPage(destination);
        }

        private void BuildTab(string title, string icon, Enums.OrderStatus status, Api_PlaceViewModel model)
        {
            var orders = model.Place.Orders.Where(x => x.Status == status).ToList();
            var view = new PlaceOrders(orders)
            {
                Title = title
            };

            if (Device.RuntimePlatform == Device.iOS)
                view.Icon = icon;

            BarBackgroundColor = Color.Black;

            var center = LayoutOptions.Center;

            if (!orders.Any())
                view.Content = new StackLayout
                {
                    VerticalOptions = center,
                    HorizontalOptions = center,
                    Children = {
                        new Image()
                        {
                            Source = "Content/Icono_Error.png",
                            HeightRequest = 200,
                            WidthRequest = 200,
                            HorizontalOptions = center,
                            VerticalOptions = center
                        },
                        new Label()
                        {
                            FontSize = 24,
                            Text = "OOPS!",
                            VerticalOptions = center,
                            HorizontalOptions = center,
                            Margin = new Thickness(0,10,0,15),
                            TextColor = Color.FromHex("FF4D5D"),
                            FontAttributes = FontAttributes.Bold,
                        },
                        new Label()
                        {
                            Text = "There's no content here yet.",
                            HorizontalTextAlignment = TextAlignment.Center,
                        },
                        new Label()
                        {
                            Text = "Please try again later.",
                            HorizontalTextAlignment = TextAlignment.Center,
                        }
                    }
                };

            Children.Add(view);
        }
    }
}