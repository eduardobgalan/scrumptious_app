﻿using Restaurant_App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Restaurant_App.Views.Place
{
    public partial class NewPlace_2 : ContentPage
    {
        List<Category> categories = new List<Category>();

        public NewPlace_2()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            this.Title = "NEW PLACE";
            this.BackgroundColor = Color.White;

            var model = (Models.Place)this.BindingContext;

            categories = await App.Manager.GetCategories();

            var counter = 0;

            if (model.ParentCategories != null)
            {
                foreach (var item in categories)
                {
                    if (model.ParentCategories.SingleOrDefault(x => x.CategoryId == item.CategoryId) != null)
                    {
                        item.RadioImage = "Content/Icono_RadioChecked.png";
                        item.IsSelected = true;
                        counter++;
                    }
                    else
                    {
                        item.RadioImage = "Content/Icono_RadioUnchecked.png";
                    }
                }
            } else
            {
                foreach (var item in categories)
                {
                    item.RadioImage = "Content/Icono_RadioUnchecked.png";
                }
            }
                 
            model.SelectedCounter = counter;

            Selected.Text = "Selected: " + model.SelectedCounter + "/" + categories.Count;

            CategoriesList.ItemsSource = categories;
        }

        async void On_Done(object sender, EventArgs e)
        {
            var model = (Models.Place)BindingContext;

            List<Int32> catList = new List<Int32>();
            var items = (List<Category>)categories.Where(x => x.IsSelected == true).ToList();

            foreach (var item in items)
            {
                catList.Add(item.CategoryId);
            }

            model.Categories = catList;

            var place = await App.Manager.ManagePlace(model);

            var destination = new PlaceCreated();
            Application.Current.MainPage = new MainPage(destination);
        }

        void On_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var content = (Models.Place)this.BindingContext;
            var model = (Category)e.SelectedItem;
            List<Category> dataList = new List<Category>();

            var cat = (Category)categories.SingleOrDefault(x => x.CategoryId == model.CategoryId);

            if (cat.RadioImage == "Content/Icono_RadioUnchecked.png")
            {
                cat.RadioImage = "Content/Icono_RadioChecked.png";
                cat.IsSelected = true;
                content.SelectedCounter += 1;
            } else
            {
                cat.RadioImage = "Content/Icono_RadioUnchecked.png";
                cat.IsSelected = false;
                content.SelectedCounter -= 1;
            }

            Selected.Text = "Selected: " + content.SelectedCounter + "/" + categories.Count;
        }

        void On_Back(object sender, EventArgs e)
        {
            var items = (List<Category>)categories.Where(x => x.IsSelected == true).ToList();
            var model = (Models.Place)this.BindingContext;
            model.ParentCategories = items;

            var destination = new NewPlace_1();
            destination.BindingContext = model;

            Application.Current.MainPage = new MainPage(destination);
        }
    }
}
