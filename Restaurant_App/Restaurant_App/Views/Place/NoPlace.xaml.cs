﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Restaurant_App.Views.Place
{
	public partial class NoPlace : ContentPage
	{
		public NoPlace ()
		{
			InitializeComponent ();
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();

            this.Title = "MY PLACES";
            this.BackgroundColor = Color.White;
        }

        void New_Place(object sender, EventArgs e)
        {
            var btn = (Button)sender;

            Device.BeginInvokeOnMainThread(() => { btn.Text = "Redirecting..."; });

            var destination = new NewPlace_1();
            Application.Current.MainPage = new MainPage(destination);
        }
    }
}
